import { useContext } from "react";

import clsx from "clsx";

const Layout = ({ className, children, ...props }) => {
	return (
		<div className={clsx("relative min-h-full", className)}>{children}</div>
	);
};

export default Layout;
