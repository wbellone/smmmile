import Header from "../sections/Header";
import Footer from "../sections/Footer";
import { useContext } from "react";
import { motion, AnimatePresence } from "framer-motion";

const Layout = ({ children, ...props }) => {
	const { popup, setPopup } = useContext(UserContext);

	return <div className="relative">{children}</div>;
};

export default Layout;
