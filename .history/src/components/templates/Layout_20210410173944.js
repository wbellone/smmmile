import { useContext } from "react";

const Layout = ({ className, children, ...props }) => {
	return <div className="relative">{children}</div>;
};

export default Layout;
