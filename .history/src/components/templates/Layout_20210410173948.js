import { useContext } from "react";

import clsx from "clsx";

const Layout = ({ className, children, ...props }) => {
	return <div className="relative">{children}</div>;
};

export default Layout;
