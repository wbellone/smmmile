import { useContext } from "react";

const Layout = ({ children, ...props }) => {
	const { popup, setPopup } = useContext(UserContext);

	return <div className="relative">{children}</div>;
};

export default Layout;
