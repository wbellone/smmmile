import { useContext } from "react";

const Layout = ({ children, ...props }) => {
	return <div className="relative">{children}</div>;
};

export default Layout;
