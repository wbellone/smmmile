import Header from "../sections/Header"
import Footer from "../sections/Footer"
import UserContext from "../elements/AppContext"
import { useContext } from "react"
import { motion, AnimatePresence } from "framer-motion"

const Layout = ({ children, ...props }) => {
	const { popup, setPopup } = useContext(UserContext)

	return (
		<motion.div
			initial={{ opacity: 0 }}
			animate={{ opacity: 1 }}
			transition={{ duration: 0.4 }}
			{...props}
		>
			<AnimatePresence initial={false}>{popup && popup}</AnimatePresence>
			<motion.div>
				<motion.div
					className="relative z-50 lg:z-auto"
					initial={{ opacity: 0, y: -100, zIndex: 10 }}
					animate={{ opacity: 1, y: 0, zIndex: "inherit" }}
					transition={{ duration: 0.4, delay: 0.4 }}
				>
					<Header />
				</motion.div>

				<div className="relative">{children}</div>
				<Footer />
			</motion.div>
		</motion.div>
	)
}

export default Layout
