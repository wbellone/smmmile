import { useField } from "@formiz/core";
import clsx from "clsx";
import { motion } from "framer-motion";
import React, { useState } from "react";

const RadioInput = ({ options, title, ...props }) => {
	const {
		errorMessage,
		id,
		isValid,
		isPristine,
		isSubmitted,
		resetKey,
		setValue,
		value,
	} = useField(props);
	const [select, setSelected] = useState(false);
	const { options, title } = props;

	return (
		<div className={clsx(`demo-form-group`, "relative mt-36")}>
			<div className="mb-20">
				<label className={clsx("text-blue font-bold")}>{title}</label>
			</div>

			<div role="group" aria-labelledby="my-radio-group">
				{options.map((item, key) => {
					return (
						<div
							key={key}
							className="flex items-start mb-15 cursor-pointer"
							onClick={() => {
								setValue(item.value);
							}}
						>
							<div className="relative flex flex-shrink-0 items-center justify-center mt-3 w-20 h-20 border border-blue rounded-full">
								<motion.div
									animate={{
										scale: value == item.value ? 1 : 0.6,
										opacity: value == item.value ? 1 : 0,
									}}
									transition={{ ease: "easeIn" }}
									className="w-10 h-10 bg-blue rounded-full"
								></motion.div>
							</div>
							<p className="ml-20">{item.label}</p>
						</div>
					);
				})}
			</div>
		</div>
	);
};

export default RadioInput;
