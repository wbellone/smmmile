import { motion } from "framer-motion";
import { useField } from "@formiz/core";
import React, { useState } from "react";
import clsx from "clsx";

export default function Field(props) {
	const {
		errorMessage,
		id,
		isValid,
		isPristine,
		isSubmitted,
		resetKey,
		setValue,
		value,
	} = useField(props);
	const { label, type, required } = props;
	const [isFocused, setIsFocused] = useState(false);
	const showError = !isValid && !isFocused && (!isPristine || isSubmitted);

	let isNotEmpty = isFocused || (value && value.length > 0);

	return (
		<div
			className={clsx(
				`demo-form-group ${showError ? "is-error" : ""}`,
				"relative mt-20 md:mt-36",
			)}
		>
			<motion.div
				animate={{ top: isNotEmpty ? -30 : 12 }}
				transition={{ ease: "easeInOut" }}
				className="mb-15 md:absolute md:left-10 md:mb-0"
			>
				<label
					className={clsx(
						"demo-label transition-all ease-in-out",
						isNotEmpty ? "text-blue" : "md:text-white text-blue",
					)}
					htmlFor={id}
				>
					{label}
					{required && " *"}
				</label>
			</motion.div>

			<input
				key={resetKey}
				id={id}
				type={type || "text"}
				value={value || ""}
				className="demo-input block px-10 py-12 w-full text-white bg-blue rounded-md outline-none"
				onChange={(e) => setValue(e.target.value)}
				onFocus={() => setIsFocused(true)}
				onBlur={() => setIsFocused(false)}
				aria-invalid={!isValid}
				aria-describedby={!isValid ? `${id}-error` : null}
			/>
			{showError && (
				<div
					id={`${id}-error`}
					className="demo-form-feedback mt-5 text-red font-bold"
				>
					{errorMessage}
				</div>
			)}
		</div>
	);
}
