import { useField } from "@formiz/core";
import clsx from "clsx";
import { motion } from "framer-motion";
import React, { useState } from "react";

const RadioInput = ({ options, title, ...props }) => {
	const {
		errorMessage,
		id,
		isValid,
		isPristine,
		isSubmitted,
		resetKey,
		setValue,
		value,
	} = useField(props);
	const [select, setSelected] = useState(false);

	return (
		<div className={clsx(`demo-form-group`, "relative mt-36")}>
			<div className="mb-20">
				<label className={clsx("text-blue font-bold")}>{title}</label>
			</div>

			<div role="group" aria-labelledby="my-radio-group"></div>
		</div>
	);
};

export default RadioInput;
