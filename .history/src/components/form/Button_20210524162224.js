import clsx from "clsx";

export default function Button({ className, type, disabled, ...props }) {
	return (
		<button
			className={clsx(
				"demo-button is-full mx-10 px-28 py-15 w-150 text-white bg-blue rounded-md",
				className,
			)}
			type={type}
			disabled={disabled}
			{...props}
		/>
	);
}
