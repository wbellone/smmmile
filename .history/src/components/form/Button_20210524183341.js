import clsx from "clsx";

const Button = ({ className, type, disabled, ...props }) => {
	return (
		<button
			className={clsx(
				"demo-button is-full min-w-150 mx-10 px-28 py-15 text-white bg-blue rounded-md",
				className,
			)}
			type={type}
			disabled={disabled}
			{...props}
		/>
	);
};

export default Button;
