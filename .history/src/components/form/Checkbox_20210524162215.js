import { useField } from "@formiz/core";
import { useEffect } from "react";
import clsx from "clsx";
import { motion } from "framer-motion";

export default function Checkbox(props) {
	const {
		errorMessage,
		id,
		isValid,
		isPristine,
		isSubmitted,
		resetKey,
		setValue,
		value = [],
	} = useField(props);

	useEffect(() => {
		setValue([]);
	}, []);

	const options = [
		{
			label: "Mediation",
			value: "Mediation",
		},
		{
			label: "Brigade Verte",
			value: "Brigade Verte",
		},
		{
			label: "Orientation",
			value: "Orientation",
		},
	];

	const updateChoices = (label) => {
		const index = value.indexOf(label);
		if (index < 0) {
			setValue([...value, label]);
		} else {
			let newArray = [...value];
			newArray.splice(index, 1);

			setValue(
				value.filter(function (item) {
					return item !== label;
				}),
			);
		}
	};

	return (
		<div className={clsx(`demo-form-group`, "relative mt-36")}>
			<div className="mb-20">
				<label className={clsx("text-blue font-bold")}>
					Quelle équipe aimerais-tu intégrer sur le Festival ? (3 choix possible
					maximum) *
				</label>
			</div>

			<div role="group" aria-labelledby="my-radio-group">
				{options.map((item, key) => {
					return (
						<div
							className="flex items-start mb-15 cursor-pointer"
							key={key}
							onClick={() => {
								updateChoices(item.value);
							}}
						>
							<div className="relative flex flex-shrink-0 items-center justify-center w-20 h-20 border border-blue">
								<motion.div
									// animate={{
									// 	scale: value == item.value ? 1 : 0.6,
									// 	opacity: value == item.value ? 1 : 0,
									// }}
									// transition={{ ease: "easeIn" }}
									className={clsx(
										"tick relative w-full h-full",
										value && value.indexOf(item.value) > -1 ? "checked" : "",
									)}
								></motion.div>
							</div>
							<p className="ml-20">{item.label}</p>
						</div>
					);
				})}
			</div>
		</div>
	);
}
