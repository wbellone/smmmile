import { useField } from "@formiz/core";

export default function RadioInput(props) {
	const {
		errorMessage,
		id,
		isValid,
		isPristine,
		isSubmitted,
		resetKey,
		setValue,
		value,
	} = useField(props);
	const [select, setSelected] = useState(false);

	const options = [
		{
			label:
				"Je suis disponible dès maintenant pour aider à l'organisation du festival en amont",
			value:
				"Je suis disponible dès maintenant pour aider à l'organisation du festival en amont",
		},
		{
			label: "Je ne peux être bénévole que le(s) jour(s) du festival",
			value: "Je ne peux être bénévole que le(s) jour(s) du festival",
		},
		{
			label: "Pas dispo cette année mais motivé(e) pour la prochaine édition!",
			value: "Pas dispo cette année mais motivé(e) pour la prochaine édition!",
		},
	];

	return (
		<div className={clsx(`demo-form-group`, "relative mt-36")}>
			<div className="mb-20">
				<label className={clsx("text-blue font-bold")}>
					Comment souhaites-tu t'investir ?
				</label>
			</div>

			<div role="group" aria-labelledby="my-radio-group">
				{options.map((item, key) => {
					return (
						<div
							key={key}
							className="flex items-start mb-15 cursor-pointer"
							onClick={() => {
								setValue(item.value);
							}}
						>
							<div className="relative flex flex-shrink-0 items-center justify-center mt-3 w-20 h-20 border border-blue rounded-full">
								<motion.div
									animate={{
										scale: value == item.value ? 1 : 0.6,
										opacity: value == item.value ? 1 : 0,
									}}
									transition={{ ease: "easeIn" }}
									className="w-10 h-10 bg-blue rounded-full"
								></motion.div>
							</div>
							<p className="ml-20">{item.label}</p>
						</div>
					);
				})}
			</div>
		</div>
	);
}
