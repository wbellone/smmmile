import clsx from "clsx";
const variantMap = {
	default: "",
	white: "text-white",
	green: "text-green",
};
const sizeMap = {
	sm: "text-sm leading-relaxed",
	md: "text-lg",
	large: "text-2xl",
};

const Text = ({ variant = "default", size = "md", className, ...props }) => {
	return (
		<div
			className={clsx(variantMap[variant], sizeMap[size], className)}
			{...props}
		/>
	);
};

export default Text;
