const Intro = () => {
	return (
		<div className={}>
			<p>
				Cher·e futur·e SMMMILER, C'est parti pour la cinquième édition du
				SMMMILE - Vegan Pop festival, qui aura lieu les 19 et 20 septembre 2020
				au Parc de la Villette, à Paris ! Tu ne connais pas encore le SMMMILE ?
				Co-produit par La Villette et en partenariat notamment avec Enercoop,
				Yogi Tea et KissKissBankBank, le SMMMILE est un festival ouvert à
				tou.te.s: amoureux.ses de musique, véganes convaincu.e.s,
				végé-curieux.ses, écolos, gourmand.e.s, adeptes de la slow life ou toute
				personne désireuse de passer un bon moment. Au SMMMILE il y a des
				concerts, mais aussi des tables rondes, des projections, des exposants &
				associations, un espace d'activités pour les enfants, de la vegan street
				food etc... Si tu veux nous aider à faire de ce festival une réalité et
				rejoindre une équipe de SMMMILERS, tu te trouves au bon endroit :) Que
				tu aies envie de t'impliquer en amont, pour la préparation du festival
				et/ou pendant les 2 jours de sa tenue, nous avons besoins de bras! Si tu
				n'as que des matinées, des après-midi ou seulement quelques heures de
				disponibles, c'est déjà super, tu pourras nous le préciser. Ce
				questionnaire nous permettra de mieux comprendre tes envies et d'établir
				le planning du festival selon tes disponibilités! Ce que nous avons à
				t'offrir en échange de ton temps, tu nous demandes ? Des goodies signés
				SMMMILE, entre autres choses...Mais c'est aussi et surtout une
				expérience riche au cœur d'un festival atypique et bienveillant qui te
				permettra de faire partie de l'aventure SMMMILE et de rencontrer des
				gens sympas qui partagent tes valeurs (et pourquoi pas continuer à
				t'impliquer dans celle-ci dans le futur ?) ! Les repas seront offerts
				aux personnes mobilisées sur les horaires du repas du midi ou soir. Dans
				l’attente de ta réponse, et au plaisir de te retrouver prochainement !
				PS : Si tu as des questions on sera ravis d'y répondre :) Envoie nous un
				petit mail ici : smmmiler@smmmilefestival.com
			</p>
		</div>
	);
};

export default Intro;
