import clsx from "clsx";

const variantBase = {
	default: "text-blue",
};

const Title = ({ variant = "default", size = "md", className, ...props }) => {
	return (
		<h1
			className={clsx(
				"leading-extra-tight text-4xl lg:text-5xl",
				variantBase[variant],
				className,
			)}
			{...props}
		/>
	);
};

export default Title;
