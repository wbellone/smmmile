import clsx from "clsx"

const variantBase = {
	default: "text-orange",
	primary: "text-green"
}

const Title = ({ variant = "default", size = "md", className, ...props }) => {
	return (
		<h1
			className={clsx(
				"text-4xl leading-extra-tight lg:text-5xl",
				variantBase[variant],
				className
			)}
			{...props}
		/>
	)
}

export default Title
