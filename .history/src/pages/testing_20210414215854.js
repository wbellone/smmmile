export default function Page({ data }) {
	const response = async () => {
		await fetch("/api/test", {
			method: "POST",
			body: JSON.stringify({}),
		});
	};
	return (
		<div>
			<button onClick={response}>Click here</button>
		</div>
	);
}
