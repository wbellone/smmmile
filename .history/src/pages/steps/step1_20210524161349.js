import { FormizStep } from "@formiz/core";
import { isEmail } from "@formiz/validations";
import clsx from "clsx";
import Field from "../../components/form/Field";

export default function Step1() {
	return (
		<FormizStep name="step1">
			<div className="my-32">
				<label className={clsx("text-blue font-bold")}>
					Informations générales
				</label>
			</div>
			<Field name="lastname" label="Nom" required="Le nom est obligatoire" />
			<Field name="firstname" label="Prénom" />
			<Field
				name="email"
				label="Email"
				type="email"
				required="Email is required"
				validations={[
					{
						rule: isEmail(),
						message: "Not a valid email",
					},
				]}
			/>
			<Field name="phone" label="Numéro de téléphone" type="number" />
		</FormizStep>
	);
}
