export default function Step1() {
	return (
		<FormizStep name="step1">
			<div className="my-32">
				<label className={clsx("text-blue font-bold")}>
					Informations générales
				</label>
			</div>
			<MyField name="lastname" label="Nom" required="Le nom est obligatoire" />
			<MyField name="firstname" label="Prénom" />
			<MyField
				name="email"
				label="Email"
				type="email"
				required="Email is required"
				validations={[
					{
						rule: isEmail(),
						message: "Not a valid email",
					},
				]}
			/>
			<MyField name="phone" label="Numéro de téléphone" type="number" />
		</FormizStep>
	);
}
