import { FormizStep } from "@formiz/core";
import Checkbox from "../../components/form/Checkbox";
import RadioInput from "../../components/form/RadioInput";

import Field from "../../components/form/Field";
import TextArea from "../../components/form/Textarea";

const options = [
	{
		label: "Oui !",
		value: "Oui",
	},
	{
		label: "Non, pas trop",
		value: "Non",
	},
];

const options_tshirt = [
	{
		label: "S",
		value: "S",
	},
	{
		label: "M",
		value: "M",
	},
	{
		label: "L",
		value: "L",
	},
];

export default function Step4() {
	return (
		<FormizStep name="step3">
			<div className="my-32">
				<label className="text-blue font-bold">Le Festival</label>
			</div>
			<Checkbox
				name="teams"
				options={options}
				title="Quelle équipe aimerais-tu intégrer sur le Festival ? (3 choix possible maximum)"
			/>
			<div className="text-sm opacity-70">
				On fait notre maximum pour vous intégrez dans les équipes de votre choix
				mais ce n'est pas toujours possible
			</div>
			<RadioInput name="howTo" />
			<TextArea
				name="motivations"
				label="Quelles sont tes motivations pour rejoindre l'équipe du SMMMILE ? "
			/>
			<Field
				name="experience"
				label="As-tu déjà été bénévole sur un festival ?"
				required="Champs obligatoire"
			/>
			<TextArea name="others" label="Envie de nous en dire plus ?" />
			<RadioInput
				name="meeting"
				options={options}
				title=".... chaud·e pour une soirée SMMMILERS ?"
			/>
			{/* <RadioInput
				name="tshirt"
				options={options_tshirt}
				title="Quelle est ta taille ?"
			/> */}
			<TextArea
				name="affiliation"
				label="Comment es-tu tombé sur ce formulaire ? (groupe facebook, ami·e...)"
			/>
		</FormizStep>
	);
}
