
export default function Step1() {

	return (
		<Layout className="bg-white">
			<MainContainer>
				<div className="m-auto pt-56 max-w-form">
					{/* <div></div> */}
					<MyForm />
				</div>
			</MainContainer>
		</Layout>
	);
}
