import { FormizStep } from "@formiz/core";
import clsx from "clsx";
import Checkbox from "../../components/form/Checkbox";
import RadioInput from "../../components/form/RadioInput";

const options = [
	{
		label: "Mediation",
		value: "Mediation",
	},
	{
		label: "Brigade Verte",
		value: "Brigade Verte",
	},
	{
		label: "Orientation",
		value: "Orientation",
	},
];

export default function Step3() {
	return (
		<FormizStep name="step3">
			<div className="my-32">
				<label className="text-blue font-bold">Le bénévole</label>
			</div>
			<TextArea
				name="motivations"
				label="Quelle équipe aimerais-tu intégrer sur le Festival ? (3 choix possible maximum)"
			/>
			<Checkbox name="job" />
			{/* <RadioInput name="howTo" /> */}
		</FormizStep>
	);
}
