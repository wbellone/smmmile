import { useEffect, useState } from "react";
import confetti from "canvas-confetti";
import { motion } from "framer-motion";

export default function Confirmation({ popinVisibility }) {
	const showConfetti = () => {
		var count = 200;
		var defaults = {
			origin: { y: 0.7 },
		};

		function fire(particleRatio, opts) {
			confetti(
				Object.assign({}, defaults, opts, {
					particleCount: Math.floor(count * particleRatio),
				}),
			);
		}

		fire(0.25, {
			spread: 26,
			startVelocity: 55,
		});
		fire(0.2, {
			spread: 60,
		});
		fire(0.35, {
			spread: 100,
			decay: 0.91,
			scalar: 0.8,
		});
		fire(0.1, {
			spread: 120,
			startVelocity: 25,
			decay: 0.92,
			scalar: 1.2,
		});
		fire(0.1, {
			spread: 120,
			startVelocity: 45,
		});
		setTimeout(() => {
			confetti({
				particleCount: 100,
				spread: 70,
				origin: { y: 0.4, x: 0.3 },
			});
		}, 200);
	};
	useEffect(() => {
		if (popinVisibility) {
			showConfetti();
		}
	}, [popinVisibility]);

	const variants = {
		open: { opacity: 1, zIndex: 0 },
		closed: { opacity: 0, zIndex: -10 },
	};
	const item = {
		open: { opacity: 1, y: 0 },
		closed: { opacity: 0, y: -200 },
		transition: { ease: "easeIn", duration: 10 },
	};

	return (
		<motion.div
			className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-30"
			animate={popinVisibility ? "open" : "closed"}
			variants={variants}
		>
			<motion.div
				className="p-20 w-450 bg-white rounded-lg"
				animate={popinVisibility ? "open" : "closed"}
				variants={item}
				transition={{ ease: "easeIn", duration: 10 }}
			>
				<p className="pb-20 text-center text-blue text-4xl font-bold">
					Merci !
				</p>
				<p>
					Votre formulaire a bien été enregistré. Nous reviendrons vers vous au
					plus vite.
					<br />
					En attendant vous pouvez suivre nos actualité sur notre page Facebook
					et Instagram.
					<br />
					<br /> A très vite, l'équipe Smmmile
				</p>
			</motion.div>
		</motion.div>
	);
}
