import { FormizStep } from "@formiz/core";
import clsx from "clsx";

export default function Step2() {
	return (
		<FormizStep name="step2">
			<Checkbox name="job" />
			<RadioInput name="howTo" />
		</FormizStep>
	);
}
