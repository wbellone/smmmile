import { useState } from "react";

export default function Confirmation() {
	const [showConfirmation, setShowConfirmation] = useState(false);
	return (
		<motion.div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-30">
			<div className="p-20 w-450 bg-white rounded-lg">
				<p className="pb-20 text-center text-blue text-4xl font-bold">
					Merci !
				</p>
				<p>
					Votre formulaire a bien été enregistré. Nous reviendrons vers vous au
					plus vite.
					<br />
					En attendant vous pouvez suivre nos actualité sur notre page Facebook
					et Instagram.
					<br />
					<br /> A très vite, l'équipe Smmmile
				</p>
			</div>
		</motion.div>
	);
}
