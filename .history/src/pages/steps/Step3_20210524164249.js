import { FormizStep } from "@formiz/core";
import clsx from "clsx";
import Checkbox from "../../components/form/Checkbox";
import RadioInput from "../../components/form/RadioInput";

import Field from "../../components/form/Field";
import TextArea from "../../components/form/Textarea";

const options = [
	{
		label: "Les jours du Festival",
		value: "Les jours du Festival",
	},
	{
		label: "Quelques jours avant le festival",
		value: "Quelques jours avant le festival",
	},
	{
		label: "Dès Maintenant",
		value: "Dès Maintenant",
	},
];

export default function Step3() {
	return (
		<FormizStep name="step3">
			<div className="my-32">
				<label className="text-blue font-bold">Le bénévole</label>
			</div>
			<TextArea
				name="motivations"
				label="Quelles sont tes motivations pour rejoindre l'équipe du SMMMILE ? "
			/>
			<Field
				name="experience"
				label="As-tu déjà été bénévole sur un festival ?"
				required="Champs obligatoire"
			/>
			<Checkbox name="availability" options={options} title="" />
			{/* <RadioInput name="howTo" /> */}
		</FormizStep>
	);
}
