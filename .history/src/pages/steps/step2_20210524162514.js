import { FormizStep } from "@formiz/core";

export default function Step2() {
	return <FormizStep name="step2"></FormizStep>;
}
