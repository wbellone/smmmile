import { FormizStep } from "@formiz/core";
import clsx from "clsx";

export default function Intro() {
	return (
		<FormizStep name="intro">
			<div className="my-32">
				<label className={clsx("text-blue font-bold")}>
					Rejoins l'équipe du SMMMILE #6
				</label>
			</div>
			<p>
				Cher·e futur·e SMMMILER,
				<br />
				<br />
				C'est parti pour la cinquième édition du SMMMILE - Vegan Pop festival,
				qui aura lieu les 18 et 19 septembre 2021 au Parc de la Villette, à
				Paris !<br />
				<br />
				Tu ne connais pas encore le SMMMILE ?<br />
				Co-produit par La Villette, le SMMMILE est un festival ouvert à
				tou.te.s: amoureux.ses de musique, véganes convaincu.e.s,
				végé-curieux.ses, écolos, gourmand.e.s, adeptes de la slow life ou toute
				personne désireuse de passer un bon moment. Au SMMMILE il y a des
				concerts, mais aussi des tables rondes, des projections, des exposants &
				associations, un espace d'activités pour les enfants, de la vegan street
				food etc...
				<br />
				<br />
				Si tu veux nous aider à faire de ce festival une réalité et rejoindre
				une équipe de SMMMILERS, tu te trouves au bon endroit :)
				<br /> Que tu aies envie de t'impliquer en amont, pour la préparation du
				festival et/ou pendant les 2 jours de sa tenue, nous avons besoins de
				bras !<br />
				Ce questionnaire nous permettra de mieux comprendre tes envies et
				d'établir le planning du festival selon les disponibilités de chacun.e.s
				! Ce que nous avons à t'offrir en échange de ton temps, tu nous demandes
				? Des goodies signés SMMMILE, entre autres choses...Mais c'est aussi et
				surtout une expérience riche au cœur d'un festival atypique et
				bienveillant qui te permettra de faire partie de l'aventure SMMMILE et
				de rencontrer des gens sympas qui partagent tes valeurs (et pourquoi pas
				continuer à t'impliquer dans celle-ci dans le futur ?) !<br />
				<br />
				Les repas seront offerts aux personnes mobilisées sur les horaires du
				repas du midi ou soir. Dans l’attente de ta réponse, et au plaisir de te
				retrouver prochainement !
			</p>
		</FormizStep>
	);
}
