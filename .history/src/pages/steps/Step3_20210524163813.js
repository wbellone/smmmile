import { FormizStep } from "@formiz/core";
import clsx from "clsx";
import Checkbox from "../../components/form/Checkbox";
import RadioInput from "../../components/form/RadioInput";

export default function Step3() {
	return (
		<FormizStep name="step3">
			<TextArea
				name="motivations"
				label="Quelle équipe aimerais-tu intégrer sur le Festival ? (3 choix possible maximum)"
			/>
			<Checkbox name="job" />
			<RadioInput name="howTo" />
		</FormizStep>
	);
}
