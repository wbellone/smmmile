import { FormizStep } from "@formiz/core";
import Checkbox from "../../components/form/Checkbox";

import Field from "../../components/form/Field";
import TextArea from "../../components/form/Textarea";

const options = [
	{
		label: "Team Brigade verte/Covid",
		value: "Team Brigade verte/Covid",
	},
	{
		label: "Team Accueil/billetterie",
		value: "Team Accueil/billetterie",
	},
	{
		label: "Team Conférence",
		value: "Team Conférence",
	},
	{
		label: "Team Bar",
		value: "Team Bar",
	},
	{
		label: "Team Little Smmmile",
		value: "Team Little Smmmile",
	},
	{
		label: "Team Scénographie (weekends avant le festival)",
		value: "Team Scénographie (weekends avant le festival)",
	},
];

export default function Step4() {
	return (
		<FormizStep name="step3">
			<div className="my-32">
				<label className="text-blue font-bold">Le bénévole</label>
			</div>
			<TextArea
				name="motivations"
				label="Quelles sont tes motivations pour rejoindre l'équipe du SMMMILE ? "
			/>
			<Field
				name="experience"
				label="As-tu déjà été bénévole sur un festival ?"
				required="Champs obligatoire"
			/>
			<Checkbox
				name="availability"
				options={options}
				title="Quelle équipe aimerais-tu intégrer sur le Festival ? (3 choix possible maximum)"
			/>
			<TextArea name="others" label="Envie de nous en dire plus ?" />
			{/* <RadioInput name="howTo" /> */}
		</FormizStep>
	);
}
