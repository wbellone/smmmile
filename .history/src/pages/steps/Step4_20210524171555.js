import { FormizStep } from "@formiz/core";
import Checkbox from "../../components/form/Checkbox";
import RadioInput from "../../components/form/RadioInput";

import Field from "../../components/form/Field";
import TextArea from "../../components/form/Textarea";

const TeamOptions = [
	{
		label: "Team Brigade verte/Covid",
		value: "Team Brigade verte/Covid",
	},
	{
		label: "Team Accueil/billetterie",
		value: "Team Accueil/billetterie",
	},
	{
		label: "Team Conférence",
		value: "Team Conférence",
	},
	{
		label: "Team Bar",
		value: "Team Bar",
	},
	{
		label: "Team Little Smmmile",
		value: "Team Little Smmmile",
	},
	{
		label: "Team Scénographie (weekends avant le festival)",
		value: "Team Scénographie (weekends avant le festival)",
	},
];

const options = [
	{
		label: "Oui !",
		value: "Oui",
	},
	{
		label: "Non, pas trop",
		value: "Non",
	},
];

const options_tshirt = [
	{
		label: "S",
		value: "S",
	},
	{
		label: "M",
		value: "M",
	},
	{
		label: "L",
		value: "L",
	},
];
const limit = 3,;

export default function Step4() {
	return (
		<FormizStep name="step4">
			<div className="my-32">
				<label className="text-blue font-bold">Le Festival</label>
			</div>
			<Checkbox
				name="teams"
				options={TeamOptions}
				limit={3}
				title="Quelle équipe aimerais-tu intégrer sur le Festival ? (3 choix possible maximum)"
			/>
			<div className="text-sm opacity-70">
				On fait notre maximum pour vous intégrez dans les équipes de votre choix
				mais ce n'est pas toujours possible
			</div>
			<RadioInput
				name="meeting"
				options={options}
				title=".... chaud·e pour une soirée SMMMILERS ?"
			/>
			<RadioInput
				name="tshirt"
				options={options_tshirt}
				title="Quelle est ta taille ?"
			/>
			<TextArea
				name="motivations"
				label="Quelles sont tes motivations pour rejoindre l'équipe du SMMMILE ? "
			/>
		</FormizStep>
	);
}
