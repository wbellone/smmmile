import { FormizStep } from "@formiz/core";
import Checkbox from "../../components/form/Checkbox";
import RadioInput from "../../components/form/RadioInput";

import Field from "../../components/form/Field";
import TextArea from "../../components/form/Textarea";

const TeamOptions = [
	{
		label: "Team Brigade verte/Covid",
		value: "Team Brigade verte/Covid",
	},
	{
		label: "Team Accueil/billetterie",
		value: "Team Accueil/billetterie",
	},
	{
		label: "Team Conférence",
		value: "Team Conférence",
	},
	{
		label: "Team Bar",
		value: "Team Bar",
	},
	{
		label: "Team Little Smmmile",
		value: "Team Little Smmmile",
	},
	{
		label: "Team Scénographie (weekends avant le festival)",
		value: "Team Scénographie (weekends avant le festival)",
	},
];

const options = [
	{
		label: "Oui !",
		value: "Oui",
	},
	{
		label: "Non, pas trop",
		value: "Non",
	},
];

const options_tshirt = [
	{
		label: "S",
		value: "S",
	},
	{
		label: "M",
		value: "M",
	},
	{
		label: "L",
		value: "L",
	},
];

export default function Step4() {
	return <FormizStep name="step3"></FormizStep>;
}
