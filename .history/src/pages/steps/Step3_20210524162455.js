import { FormizStep } from "@formiz/core";
import clsx from "clsx";
import Checkbox from "../../components/form/Checkbox";
import RadioInput from "../../components/form/RadioInput";

export default function Step2() {
	return (
		<FormizStep name="step2">
			<Checkbox name="job" />
			<RadioInput name="howTo" />
		</FormizStep>
	);
}
