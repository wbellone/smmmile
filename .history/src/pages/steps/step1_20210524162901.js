import { FormizStep } from "@formiz/core";

import clsx from "clsx";
import Field from "../../components/form/Field";

export default function Step1() {
	return (
		<FormizStep name="step1">
			<div className="my-32">
				<label className={clsx("text-blue font-bold")}>État civil</label>
			</div>
			<Field
				name="firstname"
				label="Ton prénom"
				required="Le prénom est obligatoire"
			/>
			<Field
				name="lastname"
				label="Ton nom"
				required="Le nom est obligatoire"
			/>
			<Field
				name="birthdate"
				label="Ta date de naissance"
				required="La date de naissance est obligatoire"
			/>
		</FormizStep>
	);
}
