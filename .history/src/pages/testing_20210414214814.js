const response = await fetch("/api/submit", {
    method: "POST",
    body: JSON.stringify({ id, ...values }),
  });