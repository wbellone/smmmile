import { GraphQLClient } from "graphql-request";

export default async ({ body }, res) => {
	const { id, ...data } = JSON.parse(body);

	const graphcms = new GraphQLClient(process.env.GRAPHCMS_ENDPOINT, {});

	try {
		const { createSubmission } = await graphcms.request(
			`
            mutation createVolunteer(
                input: { 
                  data: { 
                    firstname: "John",
                    lastname: "doe" ,
                    email:"john@doe.com",
                    answers:[{Label: "test", Value:"Test"}]
                  } 
                }
              ) { 
                volunteer 
                { firstname 
                  lastname
                }
              } `,
			{
				"",
				"",
			},
		);

		res.status(201).json(createSubmission);
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};
