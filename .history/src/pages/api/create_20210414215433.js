import { GraphQLClient } from "graphql-request";

export default async ({ body }, res) => {
	const { id, ...data } = JSON.parse(body);

	const firstname = "test";
	const lastname = "testing";
	const graphcms = new GraphQLClient(process.env.GRAPHCMS_ENDPOINT, {});

	try {
		const { createSubmission } = await graphcms.request(
			`
            mutation createVolunteer(
                input: { 
                  data: { 
                    firstname: $firstname,
                    lastname: $lastname,
                    email:"john@doe.com",
                    answers:[{Label: "test", Value:"Test"}]
                  } 
                }
              ) { 
                volunteer 
                { firstname 
                  lastname
                }
              } `,
			{
				firstname,
				lastname,
			},
		);

		res.status(200).json(createSubmission);
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};
