const withBundleAnalyzer = require("@next/bundle-analyzer");
const withPlugins = require("next-compose-plugins");

module.exports = withPlugins(
	[withBundleAnalyzer({ enabled: process.env.ANALYZE === "true" })],
	{
		reactStrictMode: false,
		typescript: {
			ignoreBuildErrors: true,
		},
		images: {
			domains: ["localhost", "163.172.174.163"],
		},
	},
);
