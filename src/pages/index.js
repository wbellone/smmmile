import Layout from "../components/templates/Layout";
import MainContainer from "../components/elements/MainContainer";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import { Formiz, useForm } from "@formiz/core";
import Intro from "../components/steps/Intro";
import Step1 from "../components/steps/Step1";
import Step2 from "../components/steps/Step2";
import Step3 from "../components/steps/Step3";
import Step4 from "../components/steps/Step4";
import Button from "../components/form/Button";

import Confirmation from "../components/steps/Confirmation";

// 2. Create a form with multi steps & fields
const MyForm = () => {
	const myForm = useForm();
	const [popinVisibility, setPopinVisibility] = useState(false);

	const [isLoading, setIsLoading] = React.useState(false);
	const submitForm = async (values) => {
		setIsLoading(true);

		// await fetch("/api/volunteer", {
		// 	method: "POST",
		// 	body: JSON.stringify(values),
		// });

		fetch("/api/volunteer", {
			method: "POST",
			body: JSON.stringify(values),
		});

		setTimeout(() => {
			setIsLoading(false);
			setPopinVisibility(true);
		}, 4000);
	};
	// const test = async () => {
	// 	await fetch("/api/volunteer", {
	// 		method: "POST",
	// 		body: "",
	// 	});
	// };

	// useEffect(() => {
	// 	setTimeout(() => {
	// 		test();
	// 	}, 1000);
	// }, []);

	return (
		<div className="pb-52">
			<Confirmation popinVisibility={popinVisibility} />

			<div className="mb-24 mx-auto w-150">
				<img className="block w-full" src={`/logo.png`} />
			</div>
			{/* <Button
				onClick={() => {
					setPopinVisibility(true);

					// console.log("clicked");
					// console.log(popinVisibility);
				}}
			>
				Popin
			</Button> */}

			<Formiz onValidSubmit={submitForm} connect={myForm}>
				<div className="order-first xs:order-none p-2 xs:w-auto w-full text-center text-blue text-xl">
					Étape {(myForm.currentStep && myForm.currentStep.index + 1) || 0} sur{" "}
					{myForm.steps.length}
				</div>
				<form
					noValidate
					onSubmit={myForm.submitStep}
					className="demo-form"
					style={{ minHeight: "16rem" }}
				>
					<div className="demo-form__content">
						<Intro />
						{/* <Step1 />
						<Step2 />
						<Step3 />
						<Step4 /> */}
					</div>

					{/* <div className="demo-form__footer flex items-center justify-center mt-48">
						{!myForm.isFirstStep && (
							<Button
								type="button"
								className="is-primary"
								onClick={myForm.prevStep}
							>
								Précédent
							</Button>
						)}
						{myForm.isLastStep ? (
							<Button
								className="demo-button is-full is-primary"
								type="submit"
								disabled={
									isLoading || (!myForm.isValid && myForm.isStepSubmitted)
								}
							>
								{isLoading ? "Chargement..." : "Valider"}
							</Button>
						) : (
							<Button
								type="submit"
								className="is-primary"
								disabled={!myForm.isStepValid && myForm.isStepSubmitted}
							>
								Suivant
							</Button>
						)}
					</div> */}
				</form>
			</Formiz>
		</div>
	);
};

export default function Page() {
	const [inverted, setInverted] = useState(false);

	return (
		<Layout className="bg-white">
			<MainContainer>
				<div className="m-auto pt-56 max-w-form">
					<MyForm />
				</div>
			</MainContainer>
		</Layout>
	);
}
