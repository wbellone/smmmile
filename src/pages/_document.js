import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
	static async getInitialProps(ctx) {
		const initialProps = await Document.getInitialProps(ctx);
		return { ...initialProps };
	}

	render() {
		return (
			<Html>
				<Head>
					<script
						dangerouslySetInnerHTML={{
							__html: `window.$crisp=[];window.CRISP_WEBSITE_ID="9d95da0f-5666-4f5e-a823-df12d15525b8";
							(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();`,
						}}
					/>
					<meta property="og:type" content="website" />
					<meta
						name="description"
						content="Envie de rejoindre l'équipe du Smmmile? C'est par ici :)"
					/>
					<meta
						property="og:title"
						content="Formulaire d'inscription des bénévoles Smmmiles 2021"
					/>
					<meta
						property="og:description"
						content="Envie de rejoindre l'équipe du Smmmile? C'est par ici :)"
					/>
					<meta property="og:image" content="/meta_image.jpg" />
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

export default MyDocument;
