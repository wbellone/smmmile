import { GraphQLClient } from "graphql-request";
import md5 from "md5";

const mailchimpList = async (email, firstname, lastname) => {
	try {
		// 3. Fetch the environment variables.
		const LIST_ID = "64d4af086e";
		const API_KEY = "943a6a344236ad73eaf095a7289f714a-us13";

		// 4. API keys are in the form <key>-us3.
		const DATACENTER = API_KEY.split("-")[1];

		// 5. The status of 'subscribed' is equivalent to a double opt-in.

		var encodedMail = md5(email);
		// 6. Send a POST request to Mailchimp.
		const responseMail = await fetch(
			`https://${DATACENTER}.api.mailchimp.com/3.0/lists/${LIST_ID}/members/${encodedMail}?field=status`,
			{
				headers: {
					Authorization: `apikey ${API_KEY}`,
					"Content-Type": "application/json",
				},
				method: "GET",
			},
		);

		if (responseMail.status != 200) {
			const data = {
				email_address: email,
				status: "subscribed",
				merge_fields: {
					FNAME: firstname,
					LNAME: lastname,
				},
			};

			// 6. Send a POST request to Mailchimp.
			const response = await fetch(
				`https://${DATACENTER}.api.mailchimp.com/3.0/lists/${LIST_ID}/members`,
				{
					body: JSON.stringify(data),
					headers: {
						Authorization: `apikey ${API_KEY}`,
						"Content-Type": "application/json",
					},
					method: "POST",
				},
			);

			// 7. Swallow any errors from Mailchimp and return a better error message.
			if (response.status >= 400) {
				return false;
			} else {
				return true;
			}

			// 8. If we made it this far, it was a success! 🎉
			return res.status(201).json({ error: "" });
		} else {
			return false;
		}
	} catch (error) {
		return false;
	}
};

export default async ({ body }, res) => {
	const { firstname, lastname, email, validation, ...data } = JSON.parse(body);

	console.log(JSON.parse(body));

	const sgmailer = require("@sendgrid/mail");
	sgmailer.setApiKey(process.env.SENDGRID_API_KEY);
	sgmailer.setSubstitutionWrappers("{{", "}}");

	const msg = {
		to: email,
		from: "smmmiler@smmmilefestival.com",
		templateId: "d-32c17683963e4e57be43405a92a639af",
		dynamic_template_data: {
			name: firstname,
		},
	};
	sgmailer
		.send(msg)
		.then(() => {
			res.status(200);
		})
		.catch((error) => {
			console.error(error);
		});

	const graphcms = new GraphQLClient(
		`${process.env.GRAPHCMS_ENDPOINT}/graphql`,
	);
	const args = { ...data };

	const answers = Object.keys(args).map(function (key) {
		const value = Array.isArray(args[key])
			? JSON.stringify(args[key])
			: args[key];
		return {
			Label: key,
			Value: value,
		};
	});

	const input = {
		data: {
			firstname: firstname,
			lastname: lastname,
			email: email,
			answers: answers,
		},
	};

	mailchimpList(email, firstname, lastname);

	const { createVolunteer } = await graphcms.request(
		`
		mutation createVolunteer($input: createVolunteerInput!) {
			createVolunteer(input: $input) {
			  volunteer {
				firstname
				lastname
			  }
			}
		  }	`,
		{
			input,
		},
	);

	try {
		res.status(200).json(createVolunteer);
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};

// export default function handler({ body }, res) {
// 	// const { id, ...data } = JSON.parse(body);

// }
