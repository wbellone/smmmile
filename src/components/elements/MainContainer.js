import clsx from "clsx";

const variantMap = {
	default: "max-w-main",
	small: "max-w-small",
};

const MainContainer = ({ variant = "default", className, children }) => {
	return (
		<div className={clsx(variantMap[variant], "mx-auto px-24", className)}>
			{children}
		</div>
	);
};

export default MainContainer;
