import clsx from "clsx";

const variantBase = {
	default: "text-blue",
};

const Title = ({ variant = "default", size = "md", className, ...props }) => {
	return (
		<h1
			className={clsx(
				" text-xl font-bold lg:text-3xl",
				variantBase[variant],
				className,
			)}
			{...props}
		/>
	);
};

export default Title;
