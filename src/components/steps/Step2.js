import { FormizStep } from "@formiz/core";
import Field from "../../components/form/Field";
import { isEmail } from "@formiz/validations";
import Title from "../../components/elements/Title";

export default function Step2() {
	return (
		<FormizStep name="step2">
			<div className="my-32">
				<Title>Contact</Title>
			</div>
			<Field
				name="email"
				label="Ton adresse mail"
				type="email"
				required="Email obligatoire"
				validations={[
					{
						rule: isEmail(),
						message: "Email invalide",
					},
				]}
			/>
			<Field
				name="phone"
				label="Ton Numéro de téléphone"
				type="number"
				required="Téléphone obligatoire"
			/>
			<Field name="adress" label="Ton adresse" />
		</FormizStep>
	);
}
