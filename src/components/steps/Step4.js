import { FormizStep } from "@formiz/core";
import Checkbox from "../../components/form/Checkbox";
import RadioInput from "../../components/form/RadioInput";

import Field from "../../components/form/Field";
import TextArea from "../../components/form/Textarea";
import Title from "../../components/elements/Title";
import Validation from "../form/Validation";

const TeamOptions = [
	{
		label: "Team Brigade verte",
		value: "Team Brigade verte",
	},
	{
		label: "Team Accueil/billetterie",
		value: "Team Accueil/billetterie",
	},
	{
		label: "Team Conférence / Ateliers",
		value: "Team Conférence / Ateliers",
	},
	{
		label: "Team Bar",
		value: "Team Bar",
	},
	{
		label: "Team Little Smmmile",
		value: "Team Little Smmmile",
	},
	{
		label: "Team Scénographie (weekends avant le festival)",
		value: "Team Scénographie (weekends avant le festival)",
	},
];

const options = [
	{
		label: "Oui !",
		value: "Oui",
	},
	{
		label: "Non, pas trop",
		value: "Non",
	},
];

const options_tshirt = [
	{
		label: "S",
		value: "S",
	},
	{
		label: "M",
		value: "M",
	},
	{
		label: "L",
		value: "L",
	},
];
const limit = 3;

export default function Step4() {
	return (
		<FormizStep name="step4">
			<div className="my-32">
				<Title>Le Festival</Title>
			</div>
			<Checkbox
				name="teams"
				options={TeamOptions}
				limit={limit}
				required="Champs obligatoire"
				title="Quelle équipe aimerais-tu intégrer sur le Festival ? (3 choix possible maximum)"
			/>
			<div className="text-sm opacity-70">
				Nous ferons notre maximum pour que tu sois intégré.e dans le pôle qui te
				correspond.
				<br />
				Cependant, il est possible que tu sois redirigé.e vers d'autres choix.
			</div>
			<RadioInput
				name="party"
				options={options}
				title=".... chaud·e pour une soirée SMMMILERS ?"
			/>
			<TextArea
				name="affiliation"
				required="Champs obligatoire"
				label="Comment es-tu tombé sur ce formulaire ? (groupe facebook, ami·e...)"
			/>
			<RadioInput
				name="tshirt"
				options={options_tshirt}
				required="Champs obligatoire"
				title="Quelle est ta taille ?"
			/>
			<p className="py-10">
				Pour des questions d'assurance, nous devons te faire adhérer à
				l'association :) Pour cela on demande une participation de 5€ minimum.
				Avec ces 5€, tu seras assuré·e sur tes missions et auras un T-shirt pour
				graver dans la roche ton membership SMMMILE ! La cotisation sera à
				verser par Lydia en amont du festival pour des questions de gestion, on
				t'expliquera tout très vite!
			</p>

			<Validation
				name="validation"
				required="Vous devez accepter les conditions pour continuer"
			/>
		</FormizStep>
	);
}
