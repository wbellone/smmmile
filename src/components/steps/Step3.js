import { FormizStep } from "@formiz/core";
import clsx from "clsx";
import Title from "../../components/elements/Title";
import Checkbox from "../../components/form/Checkbox";
import RadioInput from "../../components/form/RadioInput";

import Field from "../../components/form/Field";
import TextArea from "../../components/form/Textarea";

const options = [
	{
		label: "Les jours du Festival",
		value: "Les jours du Festival",
	},
	{
		label: "Quelques jours avant le festival",
		value: "Quelques jours avant le festival",
	},
	{
		label: "Dès Maintenant",
		value: "Dès Maintenant",
	},
];

const optionsVoiture = [
	{
		label: "Oui ",
		value: "Oui",
	},
	{
		label: "Non",
		value: "Non",
	},
];

export default function Step3() {
	return (
		<FormizStep name="step3">
			<div className="my-32">
				<Title>Le bénévole</Title>
			</div>
			<TextArea
				name="motivations"
				label="Quelles sont tes motivations pour rejoindre l'équipe du SMMMILE ? "
			/>
			<Field
				name="experience"
				label="As-tu déjà été bénévole sur un festival ?"
				required="Champs obligatoire"
			/>
			<RadioInput
				name="permis_voiture"
				options={optionsVoiture}
				required="Champs obligatoire"
				title="Est ce que tu possède le permis B ? ( voiture )"
			/>
			<RadioInput
				name="voiture_a_disposition"
				options={optionsVoiture}
				title="Si oui, possèdes tu un véhicule pour aider en cas de besoin ?"
			/>
			<Checkbox
				name="availability"
				options={options}
				required="Champs obligatoire"
				title="Tu es disponible : "
			/>
			<TextArea name="others" label="Envie de nous en dire plus ?" />
		</FormizStep>
	);
}
