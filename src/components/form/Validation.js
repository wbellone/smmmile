import { useField } from "@formiz/core";
import { useEffect } from "react";
import clsx from "clsx";
import { motion } from "framer-motion";
import Terms from "./Terms";

export default function Validation(props) {
	const {
		errorMessage,
		id,
		isValid,
		isPristine,
		isSubmitted,
		resetKey,
		setValue,
		value = [],
	} = useField(props);

	const { options, title, limit = 99 } = props;

	const updateChoice = (label) => {
		setValue(!value);
	};

	const showError = !isValid && value == null && (!isPristine || isSubmitted);

	return (
		<div className={clsx(`demo-form-group`, "relative mt-36")}>
			<div className="mb-20">
				<label className={clsx("text-blue font-bold")}>{title}</label>
			</div>

			<div role="group" aria-labelledby="my-radio-group">
				<div className="flex items-start mb-15">
					<div className="relative flex flex-shrink-0 items-center justify-center w-20 h-20 border border-blue">
						<motion.div
							// animate={{
							// 	scale: value == item.value ? 1 : 0.6,
							// 	opacity: value == item.value ? 1 : 0,
							// }}
							// transition={{ ease: "easeIn" }}
							className={clsx(
								"tick relative w-full h-full cursor-pointer",
								value ? "checked" : "",
							)}
							onClick={() => {
								updateChoice();
							}}
						></motion.div>
					</div>
					<p className="ml-20">
						J'ai lu et j'accepte <Terms />
					</p>
				</div>
			</div>
			{showError && (
				<div
					id={`${id}-error`}
					className="demo-form-feedback mt-5 text-red font-bold"
				>
					{errorMessage}
				</div>
			)}
		</div>
	);
}
