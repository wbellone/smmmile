import { useEffect, useState } from "react";
import { motion } from "framer-motion";

const variants = {
	visible: {
		opacity: 1,
		transition: {
			duration: 0.5,
			ease: "easeOut",
		},
	},
	hidden: {
		opacity: 0,
		transition: {
			delay: 0.4,
			duration: 0.5,
			ease: "easeOut",
		},
	},
};

const content = {
	visible: {
		opacity: 1,
		y: "0%",
		transition: {
			delay: 0.4,
			duration: 0.5,
			ease: "easeOut",
		},
	},
	hidden: {
		y: 100,
		opacity: 0,
		transition: {
			duration: 0.5,
			ease: "easeOut",
		},
	},
};

// const variants = {
// 	open: { opacity: 1, y: "0%" },
// 	closed: { opacity: 0, y: "100%" },
// };
const variantsButton = {
	show: { opacity: 1, scale: 1 },
	hide: { opacity: 0, scale: 0.8 },
};

const Terms = () => {
	const [isVisible, setIsVisible] = useState(false);

	useEffect(() => {
		document.body.style.overflow = isVisible ? "hidden" : "unset";
	}, [isVisible]);

	const Popin = ({ popinVisibility }) => {
		return (
			<>
				<motion.div
					initial="hidden"
					animate="visible"
					exit="hidden"
					variants={variantsButton}
					transition={{ ease: "easeInOut", duration: 0.7 }}
					className="fixed z-20 right-32 top-32 flex items-center justify-center w-32 h-32 border border-blue rounded-full cursor-pointer"
					onClick={() => {
						setIsVisible(false);
					}}
				>
					<p>✕</p>
				</motion.div>
				<motion.div
					className="fixed z-10 inset-0 p-40 w-full h-full bg-white overflow-scroll"
					initial="hidden"
					animate="visible"
					exit="hidden"
					variants={variants}
				>
					<motion.div
						className="m-auto max-w-main bg-white rounded-lg"
						initial="hidden"
						animate="visible"
						exit="hidden"
						// animate={popinVisibility ? "show" : "hide"}
						variants={content}
					>
						<p className="pb-20 text-center text-blue text-4xl font-bold">
							SMMMILE, l'association
						</p>
						<p>
							Lors du festival, nous appliquerons toutes les mesures nécessaires
							pour garantir votre sécurité et votre santé dans cette période si
							spéciale. C'est pourquoi nous demanderons à l'ensemble des
							bénévoles de fournir un pass sanitaire valide pendant le festival.
						</p>
						<p>
							<b>Objet :</b>
							<br />
							<br />
							cette association a pour but d’organiser des festivals de musique
							promouvant toutes les formes de transition (transition alimentaire
							vers un modèle végétal, mais aussi transition citoyenne,
							énergétique, écologique…), de produire des concerts de musique,
							des DJ-sets et des spectacles, d’organiser des événements autour
							du végétarisme/véganisme, de la protection de l’environnement et
							d’une alimentation saine (ateliers/cours/démonstrations de
							cuisine, conférences, débats, villages associatifs, espaces
							shopping, etc.), de promouvoir le travail d’artistes promouvant le
							véganisme, de développer des activités musicales et culturelles,
							de monter des projets artistiques et/ou militants autour de la
							culture et du mode de vie végane, et de conseiller tout type de
							structure (entreprises, associations, collectivités, etc.) sur
							leur stratégie et communication autour du végétarisme, du
							véganisme, d’un mode de vie sain et de l’environnement.
							<br />
							<br />
							<br />
							<b>SMMMILE Festival - Vegan & Pop</b>
							<br />
							<br />
							SMMMILE, c’est le festival du futur. Le futur que nous avons
							choisi : plus juste et plus respectueux de l’environnement, qui
							donne envie et fait rêver. Un futur vegan. SMMMILE, c’est du bon
							son, du bon sens, des happenings, des couleurs et des saveurs du
							monde entier. Un festival pour tous : véganes, végétarien·e·s,
							végé-curieux·ses, écolos, gourmand·e·s, amateur·rice·s de pop, de
							rock, d’electro... SMMMILE, un festival où l’on découvre, où l’on
							partage des moments uniques, où l’on fait autant la fête que l’on
							prend le temps de réfléchir, dans un esprit ouvert, pop, familial
							et slow.
							<br />
							<br />
							Par cette charte, l’association SMMMILE souhaite réaffirmer la
							place fondamentale de ses bénévoles au côté de l’équipe
							professionnelle et des élu·e·s de son Conseil d’Administration.{" "}
							<br />
							<br />
							<br />
							<b>Définition :</b>
							<br />
							<br />
							Au sein de l’association SMMMILE, le·a bénévole s’engage de son
							plein gré, sans toucher de rémunération et de manière
							désintéressée dans une action proposée par l’association.
							<br />
							<br />
							Les bénévoles visent à faire du bénévolat un élément
							d’épanouissement personnel, d’acquisition de connaissances et de
							compétences nouvelles, d’approfondissement de connaissances
							pré-acquises, de compétence et de capacité. Le bénévolat doit
							permettre d’être plus acteur·rice qu’usager·e et
							consommateur·rice.
							<br />
							<br />
							Le bénévolat est accessible à toute personne indépendamment des
							sexes, origines, options philosophiques ou religieuses, condition
							physique, sociale, matérielle.
							<br />
							<br />
							L’association SMMMILE s’engage à :<br />
							<br />
							- Accueillir et considérer les bénévoles comme
							collaborateur·rice·s à part entière,
							<br />
							- Donner des informations claires sur l’association, ses
							objectifs, son fonctionnement,
							<br />
							- Confier aux bénévoles une activité qui respecte leurs
							compétences, leur disponibilité et qui leur conviennent,
							<br />
							- Si besoin à assurer sa formation et son accompagnement par un·e
							responsable professionnel·le ou bénévole compétent·e,
							<br />
							- Couvrir les bénévoles par une assurance adéquate.
							<br />
							<br />
							Les bénévoles s’engagent à :<br />
							<br />
							- Accepter les principes de l’association et de se conformer à ses
							objectifs,
							<br />
							- Se sentir responsables et solidaire de la promotion et du
							développement de l’association,
							<br />
							- Accepter une préparation aux tâches (le cas échéant) et suivre
							les actions de formation proposées, <br />
							- Assurer avec sérieux, discrétion, ponctualité et régularité,
							l’activité choisie,
							<br />
							- Respecter le principe de confidentialité dans l’exercice de sa
							fonction et adhérer aux choix et à la déontologie de
							l’association,
							<br />
							- Collaborer dans un esprit de compréhension mutuelle avec les
							autres bénévoles et partenaires."
							<br />
						</p>
					</motion.div>
				</motion.div>
			</>
		);
	};

	return (
		<>
			{isVisible && <Popin />}
			<span
				onClick={() => setIsVisible(true)}
				className="font-bold cursor-pointer"
			>
				la charte du Smmmile
			</span>
		</>
	);
};

export default Terms;
