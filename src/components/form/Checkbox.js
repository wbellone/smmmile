import { useField } from "@formiz/core";
import { useEffect } from "react";
import clsx from "clsx";
import { motion } from "framer-motion";

export default function Checkbox(props) {
	const {
		errorMessage,
		id,
		isValid,
		isPristine,
		isSubmitted,
		resetKey,
		setValue,
		value = [],
	} = useField(props);

	const { options, title, limit = 99 } = props;

	useEffect(() => {
		// setValue([]);
	}, []);

	const showError = !isValid && value == null && (!isPristine || isSubmitted);

	const updateChoices = (label) => {
		if (!Array.isArray(value)) {
			setValue([label]);
		} else {
			const index = value.indexOf(label);

			if (index < 0) {
				if (value.length >= limit) {
					return;
				}
				setValue([...value, label]);
			} else {
				let newArray = value.filter(function (item) {
					return item !== label;
				});

				if (newArray.length > 0) {
					setValue(newArray);
				} else {
					setValue(false);
				}
			}
		}
	};

	return (
		<div className={clsx(`demo-form-group`, "relative mt-36")}>
			<div className="mb-20">
				<label className={clsx("text-blue font-bold")}>{title}</label>
			</div>

			<div role="group" aria-labelledby="my-radio-group">
				{options.map((item, key) => {
					return (
						<div
							className="flex items-start mb-15 cursor-pointer"
							key={key}
							onClick={() => {
								updateChoices(item.value);
							}}
						>
							<div className="relative flex flex-shrink-0 items-center justify-center w-20 h-20 border border-blue">
								<motion.div
									// animate={{
									// 	scale: value == item.value ? 1 : 0.6,
									// 	opacity: value == item.value ? 1 : 0,
									// }}
									// transition={{ ease: "easeIn" }}
									className={clsx(
										"tick relative w-full h-full",
										value && value.indexOf(item.value) > -1 ? "checked" : "",
									)}
								></motion.div>
							</div>
							<p className="ml-20">{item.label}</p>
						</div>
					);
				})}
			</div>
			{showError && (
				<div
					id={`${id}-error`}
					className="demo-form-feedback mt-5 text-red font-bold"
				>
					{errorMessage}
				</div>
			)}
		</div>
	);
}
